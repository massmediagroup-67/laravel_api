<?php


namespace App\Services;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;

class ApiClientManager
{
    const PATH = 'http://localhost:8001';

    /**
     * Passport oauth2 password authorization
     * @param $user
     *
     */
    public function auth($user)
    {
        if (Auth::check()) {
            $http = new \GuzzleHttp\Client();

            try {
                $response = $http->post(self::PATH.'/oauth/token', [
                    'form_params' => [
                        'grant_type' => 'password',
                        'client_id' => config('services.post-api.client_id'),
                        'client_secret' => config('services.post-api.client_secret'),
                        'username' => $user->email,
                        'password' => $user->password,
                        'scope' => '',
                    ],
                ]);
            } catch (ClientException $exception) {
                return;
            }
            // set token to user in database
            $user->update(json_decode((string)$response->getBody(), true));
        }
    }

    public function createPost($data)
    {
        if (Auth::check()) {
            $http = new \GuzzleHttp\Client();
            $user = Auth::user();
            $response = $http->post(self::PATH.'/api/post', [
                'headers' => [
                    'Authorization' => $user->token_type . ' ' . $user->access_token,
                ],
                'form_params' => [
                    'title' => $data['title'],
                    'description' => $data['description'],
                ] ,
            ]);

            return json_decode((string) $response->getBody(), true);
        }
        return false;
    }

    public function getPosts()
    {
        $http = new \GuzzleHttp\Client();
        $response = $http->get(self::PATH.'/api/post');
        return json_decode((string) $response->getBody(), true);
    }

    public function getPost($id)
    {
        $http = new \GuzzleHttp\Client();
        $response = $http->get(self::PATH.'/api/post/'.$id);
        return json_decode((string) $response->getBody(), true);
    }
}
