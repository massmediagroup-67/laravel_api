<?php


namespace App\Services;

use App\SocialFacebookAccount;
use App\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Contracts\User as FaceBookAccount;

class SocialFacebookAccountService
{
    public function createOrGetUser(FaceBookAccount $fb)
    {
        $account = SocialFacebookAccount::where('facebook_user_id', $fb->getId())->first();

        if ($account) {
            return $account->user;
        } else {
            $account = new SocialFacebookAccount([
                'facebook_user_id' => $fb->getId(),
            ]);

            $user = User::whereEmail($fb->getEmail())->first();

            if (!$user) {
                $user = User::create([
                    'email' => $fb->getEmail(),
                    'name' => $fb->getName(),
                    'password' => Hash::make(rand(1, 10000)),
                ]);
            }

            $user->fbAccount()->save($account);

            return $user;
        }
    }
}
