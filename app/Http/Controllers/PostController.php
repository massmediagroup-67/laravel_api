<?php

namespace App\Http\Controllers;

use App\Services\ApiClientManager;
use App\Http\Requests\PostRequest;

class PostController extends Controller
{
    public function index(ApiClientManager $cm)
    {
        return view('post.index', ['posts' => $cm->getPosts()]);
    }

    public function show(ApiClientManager $cm, $id)
    {
        return view('post.show', ['post' => $cm->getPost($id)]);
    }

    public function create()
    {
        return view('post.createForm');
    }

    public function store(ApiClientManager $cm, PostRequest $request)
    {
        $cm->createPost($request->input());
        return redirect()->route('post.index');
    }
}
