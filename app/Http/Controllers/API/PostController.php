<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @return Post[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Post::all();
    }

    /**
     * @response 200 {
     *     "id": 1,
     *     "title": "some title",
     *     "description": "some description",
     *     "created_at": "2019-05-29 07:19:30",
     *     "updated_at": "2019-06-15 08:20:00"
     * }
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return Post::find($id);
    }

    /**
     * @bodyParam title string required The title of the post.
     * @bodyParam description string required The description of the post.
     * @authenticated
     * @response 201 {
     *     "id": 1,
     *     "title": "some title",
     *     "description": "some description",
     *     "created_at": "2019-05-29 07:19:30",
     *     "updated_at": "2019-06-15 08:20:00"
     * }
     * @response 400 {
     *  "message": "Set required fields"
     * }
     * @response 401 {
     *  "message": "Unauthorized"
     * }
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        if (isset($request->title) && isset($request->description)) {
            $post = Post::create($request->all());
            return response()->json($post, 201);
        }

        return  response()->json('Set required fields', 400);
    }
}
