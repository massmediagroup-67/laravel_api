<?php

namespace App\Http\Controllers;

use App\Services\ApiClientManager;
use Laravel\Socialite\Facades\Socialite;
use App\Services\SocialFacebookAccountService;
use Illuminate\Support\Facades\Auth;

class SocialAuthFacebookController extends Controller
{
    /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @param SocialFacebookAccountService $service
     * @param ApiClientManager $cm
     * @return callback URL from facebook
     */
    public function callback(SocialFacebookAccountService $service, ApiClientManager $cm)
    {
        $user = $service->createOrGetUser(Socialite::driver('facebook')->stateless()->user());
        Auth::login($user, true);
        $cm->auth($user);

        return redirect()->route('post.index');
    }
}
