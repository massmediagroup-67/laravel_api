<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialFacebookAccount extends Model
{
    protected $primaryKey = 'facebook_user_id';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;

    protected $fillable = ['facebook_user_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
