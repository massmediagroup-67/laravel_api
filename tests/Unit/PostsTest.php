<?php

namespace Tests\Unit;

use App\Post;
use App\User;

use Tests\TestCase;

class PostsTest extends TestCase
{
    public function testCanCreatePost()
    {
        $data = [
            'title' => $this->faker->sentence,
            'description' => $this->faker->paragraph
        ];
        $token = User::first()['access_token'];
        $user = factory(User::class)->create();

        $headers = [
            'Accept' => 'application/json',
            'Authorization' => "Bearer $token",
        ];
        $this->actingAs($user, 'api')->post('http://localhost:8001/api/post', $data, $headers)
            ->assertStatus(201)
            ->assertJson($data);
    }

    public function testCanShowPost()
    {
        $post = factory(Post::class)->create();
        $this->get('http://localhost:8001/api/post/'.$post->id)
            ->assertStatus(200);
    }

    public function testCanListPosts()
    {
        $posts = factory(Post::class, 2)->create()->map(function ($post) {
            return $post->only(['id', 'title', 'description']);
        });
        $this->get('http://localhost:8001/api/post')
            ->assertStatus(200)
            ->assertJson($posts->toArray())
            ->assertJsonStructure([
                '*' => [ 'id', 'title', 'description' ],
            ]);
    }
}
