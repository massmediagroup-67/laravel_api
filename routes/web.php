<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;


Route::get('/', function () {
    return redirect()->route('post.index');
});

Route::get('/logout', function () {
    Auth::logout();
    return redirect()->route('post.index');
})->name('logout');


Route::get('/facebook/redirect', 'SocialAuthFacebookController@redirect')->name('facebook.login');
Route::get('/facebook/callback', 'SocialAuthFacebookController@callback');

Route::get('/post/index', 'PostController@index')->name('post.index');
Route::get('/post/show/{id}', 'PostController@show')->name('post.show');
Route::get('/post/create', 'PostController@create')->name('post.create')->middleware('auth');
Route::post('/post/create', 'PostController@store')->name('post.store');
