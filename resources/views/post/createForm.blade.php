@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Posts</div>

                    <div class="card-body">
                        <form method="POST" class="w-100">
                            {{ csrf_field() }}
                            @component('post.form-group', ['label' => 'Title:', 'name' => 'title'])
                                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title"
                                       name="title">
                            @endcomponent

                            @component('post.form-group', ['label' => 'Description:', 'name' => 'description'])
                                <textarea class="form-control @error('description') is-invalid @enderror"
                                          id="description" rows="3" name="description"></textarea>
                            @endcomponent

                            <button type="submit" class="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
