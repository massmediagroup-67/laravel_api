@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Posts</div>

                    <div class="card-body">
                            <div class="post">
                                <div class="title">{{ $post['title'] }}</div>
                                <div class="description">{{ $post['description'] }}</div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
