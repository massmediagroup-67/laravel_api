@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Posts</div>

                    <div class="card-body">
                        @foreach ($posts as $post)
                            <div class="post">
                                <div class="title"> <a href="{{ route('post.show',['id' => $post['id']]) }}"> {{ $post['title'] }} </a></div>
                                <div class="description">{{ $post['description'] }}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
