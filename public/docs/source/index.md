---
title: API Reference

language_tabs:
- php
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.

<!-- END_INFO -->

#general
<!-- START_0bfd90e643e49117746d270f35d4851b -->
## api/post
> Example request:

```php

$client = new \GuzzleHttp\Client();
$response = $client->get("api/post", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```bash
curl -X GET -G "http://localhost/api/post" 
```
```javascript
const url = new URL("http://localhost/api/post");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
[
    {
        "id": 5,
        "title": "Ratione ratione enim sequi qui et.",
        "description": "Quo aliquam unde vero sunt velit rerum. Veritatis adipisci facere consequatur quas et ex. Sunt natus cumque quas officiis enim autem.",
        "created_at": "2019-05-29 11:47:07",
        "updated_at": "2019-05-29 11:47:07"
    },
    {
        "id": 6,
        "title": "Omnis quasi laboriosam unde molestiae temporibus corrupti.",
        "description": "Provident placeat non nihil odio eos totam quia et. Cumque laborum voluptate eos. Et reprehenderit architecto deserunt dolor iste ea. Quam ut unde expedita et quo nulla.",
        "created_at": "2019-05-29 11:47:14",
        "updated_at": "2019-05-29 11:47:14"
    },
    {
        "id": 7,
        "title": "Est explicabo id ex pariatur.",
        "description": "Commodi numquam eveniet rerum tempora perspiciatis dolores libero. Voluptatem quo et ratione aperiam. Vel dignissimos aut eum iusto.",
        "created_at": "2019-05-29 11:47:19",
        "updated_at": "2019-05-29 11:47:19"
    }
]
```

### HTTP Request
`GET api/post`


<!-- END_0bfd90e643e49117746d270f35d4851b -->

<!-- START_60f64b50a8ac60c7eea9b2c1e1868b65 -->
## api/post/{id}
> Example request:

```php

$client = new \GuzzleHttp\Client();
$response = $client->get("api/post/1", [
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```bash
curl -X GET -G "http://localhost/api/post/1" 
```
```javascript
const url = new URL("http://localhost/api/post/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "id": 1,
    "title": "some title",
    "description": "some description",
    "created_at": "2019-05-29 07:19:30",
    "updated_at": "2019-06-15 08:20:00"
}
```

### HTTP Request
`GET api/post/{id}`


<!-- END_60f64b50a8ac60c7eea9b2c1e1868b65 -->

<!-- START_112f38c169c6b664068ce459c85b7d63 -->
## api/post
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```php

$client = new \GuzzleHttp\Client();
$response = $client->post("api/post", [
    'headers' => [
            "Authorization" => "Bearer {token}",
            "Content-Type" => "application/json",
        ],
    'json' => [
            "title" => "aliquid",
            "description" => "voluptatem",
        ],
]);
$body = $response->getBody();
print_r(json_decode((string) $body));
```

```bash
curl -X POST "http://localhost/api/post" \
    -H "Authorization: Bearer {token}" \
    -H "Content-Type: application/json" \
    -d '{"title":"aliquid","description":"voluptatem"}'

```
```javascript
const url = new URL("http://localhost/api/post");

let headers = {
    "Authorization": "Bearer {token}",
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "title": "aliquid",
    "description": "voluptatem"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (201):

```json
{
    "id": 1,
    "title": "some title",
    "description": "some description",
    "created_at": "2019-05-29 07:19:30",
    "updated_at": "2019-06-15 08:20:00"
}
```
> Example response (400):

```json
{
    "message": "Set required fields"
}
```
> Example response (401):

```json
{
    "message": "Unauthorized"
}
```

### HTTP Request
`POST api/post`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    title | string |  required  | The title of the post.
    description | string |  required  | The description of the post.

<!-- END_112f38c169c6b664068ce459c85b7d63 -->


